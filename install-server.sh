# Install sensu server in ubuntu 14.04 LTS. Admin rights required

# Install rabbitmq

echo "deb http://www.rabbitmq.com/debian/ testing main" |  tee -a /etc/apt/sources.list.d/rabbitmq.list
curl -L -o ~/rabbitmq-signing-key-public.asc http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
apt-key add ~/rabbitmq-signing-key-public.asc
apt-get update &&  apt-get install -y rabbitmq-server erlang-nox
service rabbitmq-server start
cd /tmp && wget http://sensuapp.org/docs/0.13/tools/ssl_certs.tar && tar -xvf ssl_certs.tar
cd ssl_certs && ./ssl_certs.sh generate
mkdir -p /etc/rabbitmq/ssl &&  cp /tmp/ssl_certs/sensu_ca/cacert.pem /tmp/ssl_certs/server/cert.pem /tmp/ssl_certs/server/key.pem /etc/rabbitmq/ssl
cp config-server/rabbitmq.config /etc/rabbitmq/rabbitmq.config
service rabbitmq-server restart
rabbitmqctl add_vhost /sensu
rabbitmqctl add_user sensu pass
rabbitmqctl set_permissions -p /sensu sensu ".*" ".*" ".*"


# Install redis-server

apt-get -y install redis-server
service redis-server start


# Install sensu from repo

wget -q http://repos.sensuapp.org/apt/pubkey.gpg -O- |  apt-key add -
echo "deb     http://repos.sensuapp.org/apt sensu main" |  tee -a /etc/apt/sources.list.d/sensu.list
apt-get update &&  apt-get install -y sensu uchiwa
mkdir -p /etc/sensu/ssl &&  cp /tmp/ssl_certs/client/cert.pem /tmp/ssl_certs/client/key.pem /etc/sensu/ssl


# Configure and restart sensu

cp config-server/rabbitmq.json /etc/sensu/conf.d/rabbitmq.json
cp config-server/redis.json /etc/sensu/conf.d/redis.json
cp config-server/api.json /etc/sensu/conf.d/api.json
cp config-server/uchiwa.json /etc/sensu/conf.d/uchiwa.json
cp config-server/clien.json /etc/sensu/conf.d/client.json
update-rc.d sensu-server defaults
update-rc.d sensu-client defaults
update-rc.d sensu-api defaults
update-rc.d uchiwa defaults
service sensu-server start
service sensu-client start
service sensu-api start
service uchiwa start


# Sensu dashboard accesible in localhost:3000


# Copy SSL certificates in client to monitorize (change user and ip with our clients)

scp /tmp/ssl_certs/client/cert.pem /tmp/ssl_certs/client/key.pem user@ip:/tmp


