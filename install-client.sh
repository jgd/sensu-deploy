
# Install sensu on client

http://repos.sensuapp.org/apt/pubkey.gpg -O- | sudo apt-key add -
echo "deb     http://repos.sensuapp.org/apt sensu main" | sudo tee -a /etc/apt/sources.list.d/sensu.list
sudo apt-get update && sudo apt-get -y install sensu
sudo mkdir -p /etc/sensu/ssl && sudo cp /tmp/cert.pem /tmp/key.pem /etc/sensu/ssl

# Copy config files

cp config-client/rabbitmq.json /etc/sensu/conf.d/rabbitmq.json
cp config-client/clien.json /etc/sensu/conf.d/client.json

# Start services

sudo update-rc.d sensu-client defaults
sudo service sensu-client start


# At this point sensu client suppport is activated. 
# Go to dashboard on server 